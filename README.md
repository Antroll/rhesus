# Gulp-boilerplate

#### Установка
Для работы со сборкой проэкта необходимы установленные **[node.js](https://nodejs.org/en/)**  и **[ruby](https://www.ruby-lang.org/ru/)**
Так же должны быть установлены

jade:
```sh
$ npm install jade --global
```

sass:
```sh
$ gem install sass
```

Gulp и Bower:

```sh
$ npm install -g gulp
```
#### Готовим проект
```sh
$ git clone https://gitlab.com/Antroll/new-project.git
$ cd new-project
$ npm i
```

Для просмотра проекта:

```sh
$ gulp serve
```

Исходники проекта содержаться в папке **project/dist**