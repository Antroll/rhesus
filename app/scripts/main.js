'use strict';

const myApp = {
	// сет брейкпоинтов для js
	// должны совпадать с теми что в body:after
	mediaBreakpoint: {
		sm: 576,
		md: 768,
		lg: 992,
		xl: 1200
	},
	initBefore: function() {
		this.svgIcons()
		document.documentElement.className =
			document.documentElement.className.replace("no-js", "js");
	},

	init: function() {
		this.lazyload();

		if ('objectFit' in document.documentElement.style === false) {
			this.objectFitFallback($('[data-object-fit]'));
		}

		$('.js-tel').mask('+7 (999) 999-99-99', {
			placeholder: '+7 (___) ___-__-__'
		});

		this.buttons();
		this.closeOnFocusLost();
	},

	initOnLoad: function() {
		$('.dot').dotdotdot({ watch: 'window' });
	},

	buttons: function() {
		$(document).on('click', '.menu-trigger', function() {
			$('body').toggleClass('nav-showed');
		});
	},

	closeOnFocusLost: function() {
		$(document).click(function(e) {
			const $trg = $(e.target);
			if (!$trg.closest(".header").length) {
				$('body').removeClass('nav-showed');
			}
		});
	},

	lazyload: function () {
		const app = this
		if (typeof app.myLazyLoad == 'undefined') {
			_regularInit ()
		} else {
			_update()
		}

		function _update () {
			// console.log('LazyLoad update');
			app.myLazyLoad.update();
			app.objectFitFallback($('[data-object-fit]'));
		}

		function _regularInit () {
			// console.log('LazyLoad first init');
			app.myLazyLoad = new LazyLoad({
				elements_selector: ".lazyload",
				callback_error: function(el) {
					el.parentElement.classList.add('lazyload-error')
				}
			});
			app.objectFitFallback($('[data-object-fit]'));
		}
	},

	objectFitFallback: function($selector) {
		if ('objectFit' in document.documentElement.style === false) {
			console.log('objectFit Fallback');
			$selector.each(function(i, item) {
				const $t = $(item);
				const fitStyle = $t.attr('data-object-fit');
				const imgUrl = $t.attr('src') ? $t.attr('src') : $t.attr('data-src')
				$t.parent().css({
					'backgroundImage': 'url(' + imgUrl + ')',
					'backgroundSize': fitStyle
				}).addClass('fit-img');
			});
		}
	},

	svgIcons: function () {
		const container = document.querySelector('[data-svg-path]')
		const path = container.getAttribute('data-svg-path')
		const xhr = new XMLHttpRequest()
		xhr.onload = function() {
			container.innerHTML = this.responseText
		}
		xhr.open('get', path, true)
		xhr.send()
	},

	getScreenSize: function() {
		let screenSize =
			window
			.getComputedStyle(document.querySelector('body'), ':after')
			.getPropertyValue('content');
		screenSize = parseInt(screenSize.match(/\d+/));
		return screenSize;
	}
};

myApp.initBefore();

$(function() {
	myApp.init();
});

$(window).on('load', function() {
	myApp.initOnLoad();
});
